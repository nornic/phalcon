<?php

use Phalcon\Loader;
use Phalcon\Mvc\View;
use Phalcon\Mvc\Application;
use Phalcon\Di\FactoryDefault;
use Phalcon\Db\Adapter\Pdo\Mysql as DbAdapter;
use Phalcon\Mvc\Url as UrlProvider;

// Define some absolute path constants to aid in locating resources
define('BASE_PATH', dirname(__DIR__));
define('APP_PATH', BASE_PATH . '/');

//Load configuration file
$config = require(APP_PATH . 'config/params.php');

require(APP_PATH . 'vendor/autoload.php');

$config = require(APP_PATH . 'config/params.php');

// Register an autoloader
$loader = new Loader();

$loader->registerDirs(
    [
        APP_PATH . '/controllers/',
        APP_PATH . '/models/',
    ]
);

$loader->registerNamespaces([
    'app\components' => APP_PATH.'components/',
    'app\tasks' => APP_PATH.'tasks/',

])->register();

$loader->register();

// Create a DI
$di = new FactoryDefault();

// Setup the database service
$di->set(
    'db',
    function () {
        global $config;
        return new DbAdapter(
            $config['db']
        );
    }
);

// Setup the view component
$di->set(
    'view',
    function () {
        $view = new View();
        $view->setViewsDir(APP_PATH . 'views/');
        return $view;
    }
);

// Setup a base URI
$di->set(
    'url',
    function () {
        $url = new UrlProvider();
        $url->setBaseUri('/');
        return $url;
    }
);
$application = new Application($di);

try {
    // Handle the request
    $response = $application->handle();
    $response->send();
} catch (\Exception $e) {
    echo 'Exception: ', $e->getMessage();
}
/**
 * Debug function
 *
 * @param $v
 * @param bool $exit
 */
function ea($v, $exit = true)
{
    echo '<pre>';
    var_dump($v);
    if ($exit) {
        die();
    }
}
