document.addEventListener("DOMContentLoaded", function() {
    Vue.directive('mask', VueMask.VueMaskDirective);
    const phone_book = new Vue({
        el: '#phone_book',
        data: {
            phone_list: null,
            pages:[],
            alert_message: {
                type: null,
                text: null,
            },
            errors: null,
            search_query: null,
            old_form: null,
            form: {
                id: null,
                first_name: null,
                last_name: null,
                phone_number: null,
                country_code: null,
                time_zone: null
            }
        },
        methods: {
            inputHasError(field) {
               return (this.errors && typeof this.errors[field] !== 'undefined');
            },
            getMessageClass() {
                return "alert alert-" + this.alert_message.type + " alert-dismissible fade show";
            },

            clearForm() {
                for (let key in this.form) {
                    this.form[key] = null
                }
            },

            clearAlert() {
                this.alert_message.text = null;
                this.alert_message.type = null
            },

            clearSearchQuery() {

                this.search_query = null;

            },

            openEditor(phone) {
                this.form = {
                    id: null,
                        first_name: null,
                        last_name: null,
                        phone_number: null,
                        country_code: null,
                        time_zone: null
                }
                if (phone) {
                    this.form = phone;
                }
            },

            saveCard() {
                axios
                    .post('/api/save/', {form: this.form})
                    .then(response => {
                        this.errors = null;
                        if (response.data.errors) {
                            this.errors = response.data.errors;
                        } else {
                            $('#editModal button[data-dismiss="modal"]').click();
                            this.getItems();
                        }

                        this.setAlert(response);

                        return true;
                    });
            },

            deleteItem(id) {
                axios
                    .post('/api/delete/', {id: id})
                    .then(response => {

                        this.setAlert(response);
                        this.getItems();
                        return true;
                    });
            },
            getItems(page) {
                axios
                    .post('/api/list/', {query: this.search_query, page: page})
                    .then(response => {

                        this.phone_list = [];
                        this.setAlert(response);
                        if (this.search_query) {
                            this.pages = null;
                        }

                        if (response.data.errors) {
                        } else {
                            this.pages = []
                            this.phone_list = response.data.data.content;
                            this.preparePages(response.data.data.pages);

                        }
                        return true;
                    });
            },

            preparePages(pages) {
                let c_page = null;
                for (let i = 0; i< pages.length; ++i) {
                    this.pages.push({title: pages[i], value: pages[i]})
                    if (pages[i+1] > pages[i] + 1) {
                        this.pages.push({title: '...', value: null})
                    }


                }

            },

            setAlert(response) {
                if (response.data.message) {
                    this.alert_message.text = response.data.message;
                    this.alert_message.type = response.data.type;
                }
            },

        },
        mounted() {
            this.getItems();
        }
    })
});
