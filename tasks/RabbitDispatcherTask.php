<?php

use Phalcon\Cli\Task;
use app\components\rabbitmq\RabbitDispatcherCountryCode;
use app\components\rabbitmq\RabbitDispatcherTimeZome;

/**
 * Rabbit daemons list
 *
 * Class RabbitDispatcherTask
 */
class RabbitDispatcherTask extends Task
{
    /**
     * Country code daemon
     */
    public function countryCodeAction()
    {
        RabbitDispatcherCountryCode::i()->setUpServer(getAppParams('rabbit_mq'));
    }

    /**
     * Time zone daemon
     */
    public function timeZoneAction()
    {
        RabbitDispatcherTimeZome::i()->setUpServer(getAppParams('rabbit_mq'));
    }
}