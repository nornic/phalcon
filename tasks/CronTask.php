<?php

use Phalcon\Cli\Task;
use app\components\rabbitmq\RabbitSenderTimeZone;
use app\components\rabbitmq\RabbitSenderCountryCode;

/**
 * Cron actions
 *
 * Class CronTask
 */
class CronTask extends Task
{
    /**
     * Add time zone query to rabbit queue
     */
    public function getTimeZoneAction()
    {
        (new RabbitSenderTimeZone(getAppParams('rabbit_mq')))->send();
    }

    /**
     * Add time zone country code to rabbit queue
     */
    public function getCountryCodeAction()
    {
        (new RabbitSenderCountryCode(getAppParams('rabbit_mq')))->send();

    }
}