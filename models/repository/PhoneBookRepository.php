<?php
namespace repository;

use Phalcon\Paginator\Adapter\Model as Paginator;
use PhoneBook;

/**
 * PhoneBook paged collection storage
 *
 * Class PhoneBookRepository
 * @package repository
 */
class PhoneBookRepository
{
    const PAGE_LIMIT = 14;

    /** @var null|Paginator  */
    private $paginator = null;

    public function __construct(Paginator $paginator = null)
    {
        $this->paginator = $paginator;
    }

    /**
     *
     * @param string $query
     * @return array
     */
    public function getPaginatedItemsAsArray($query = ''): array
    {
        $items = $this->paginator->getPaginate()->items ?? self::findByQuery($query);
        $result = [];
        /** @var PhoneBook $item */
        foreach ($items as $item) {
            $result[] = $item->toArray();
        }

        return $result;
    }

    /**
     * Page nums for pagination
     *
     * @param array $page_list
     * @return array
     */
    public function getPages(array $page_list = []): array
    {
        $pages = [];
        if (is_null($this->paginator)) {
            return $pages;
        }

        $p = 1;

        foreach ($page_list as $page_no) {
            if (isset($this->paginator->getPaginate()->$page_no)) {
                $pages[] = $this->paginator->getPaginate()->$page_no;
                $p = $p * $this->paginator->getPaginate()->$page_no;

                if ($p === 0) {
                    return [];
                }
            }
        }
        $pages = array_unique($pages);
        sort($pages);

        return $pages ;
    }

    /**
     * @param string|null $query
     * @return \Phalcon\Mvc\Model\ResultsetInterface
     */
    public static function findByQuery(string $query = null)
    {
        $condition_addon = "";
        if ($query) {
            $condition_addon = " AND (first_name LIKE '%$query%' OR last_name LIKE '%$query%')";
        }

        $conditions = [
            "conditions" => "is_deleted != ".PhoneBook::IS_DELETED . $condition_addon,
        ];

        return PhoneBook::find($conditions);
    }



}