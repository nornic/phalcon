<?php
use Phalcon\Mvc\Model;
use Phalcon\Validation;
use Phalcon\Validation\Validator\Regex;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\StringLength;
use Phalcon\Validation\Validator\InclusionIn;
use app\components\hostaway\TimeZone;
use app\components\hostaway\CountryCode;


class PhoneBook extends Model
{
    const IS_DELETED = 1;
    /** @var int */
    protected $id;
    /** @var string */
    protected $first_name;
    /** @var string */
    protected $last_name;
    /** @var string */
    protected $phone_number;
    /** @var string */
    protected $country_code;
    /** @var string */
    protected $time_zone;
    /** @var int */
    protected $insertedOn;
    /** @var int */
    protected $updatedOn;
    /** @var bool */
    protected $is_deleted;

    public function initialize()
    {
        $this->setSource('phone_book');
    }

    public function setDeleted()
    {
        $this->is_deleted = self::IS_DELETED;
        return $this;
    }

    public function load(array $config = []):bool
    {
        $result = false;
        foreach ($config as $field => $value) {
            if (property_exists(self::class, $field)) {
                $this->$field = $value;
                $result = true;
            }
        }
        $time = time();
        $this->insertedOn = $this->insertedOn ?? $time;
        $this->updatedOn = $time;

        return $result;
    }

    public function validation()
    {
        if ($this->is_deleted) {
            return true;
        }
        $validation = new Validation();

        $validation->add(
            'first_name',
            new PresenceOf([
                'cancelOnFail' => true,
            ])
        );

        $validation->add(
            'first_name',
            new StringLength([
                'messageMaximum' => 'Max 199 characters',
                'max'            => 199,
            ])
        );

        $validation->add(
            'last_name',
            new StringLength([
                'messageMaximum' => 'Max 199 characters',
                'max'            => 199,
            ])
        );

        $validation->add(
            'phone_number',
            new PresenceOf([
                'cancelOnFail' => true,
            ])
        );

        $validation->add(
            'phone_number',
            new Regex([
                'pattern' => '/\+[0-9]+(.|)[0-9]+/',
            ])
        );

        $validation->add(
            'phone_number',
            new StringLength([
                'messageMaximum' => 'Max 50 symbols',
                'max'            => 50,
            ])
        );

        $validation->add(
            'phone_number',
            new StringLength([
                'messageMinimum' => 'The telephone is too short',
                'min'            => 2,
            ])
        );

        $validation->add(
            'time_zone',
            new StringLength([
                'messageMaximum' => 'Max 50 symbols',
                'max'            => 50,
            ])
        );

        $validation->add(
            'time_zone',
            new InclusionIn([
                'message' => 'Wrong time zone',
                'domain' => TimeZone::getResultKeys()
        ]));

        $validation->add(
            'country_code',
            new StringLength([
                'messageMaximum' => 'Max 50 symbols',
                'max'            => 50,
            ])
        );

        $validation->add(
            'country_code',
            new InclusionIn([
                'message' => 'Wrong country code',
                'domain' => CountryCode::getResultKeys()
            ]));

        return $this->validate($validation);
    }
}