<?php


use Phalcon\Mvc\Controller;
use app\components\hostaway\TimeZone;

class IndexController extends Controller
{
    public function initialize()
    {
        $this->view->setTemplateAfter('main');
        // Add some local CSS resources
        $this->assets->addCss('//stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css');
        $this->assets->addCss('//use.fontawesome.com/releases/v5.7.2/css/all.css');

        // And some local JavaScript resources
        $this->assets->addJs('js/core/vue.js');
        $this->assets->addJs('js/core/axios.min.js');
        $this->assets->addJs('js/core/vmask.js');

        $this->assets->addJs('js/core/jquery-3.3.1.slim.min.js');
        $this->assets->addJs('js/core/bootstrap.min.js');
        $this->assets->addJs('js/core/popper.min.js');

        $this->assets->addJs('js/phone_book.js');
    }


    public function indexAction()
    {
        return $this->view->pick('index/phone_book');
    }
}