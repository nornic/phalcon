<?php
use Phalcon\Mvc\Controller;
use Phalcon\Paginator\Adapter\Model as Paginator;
use repository\PhoneBookRepository;


class ApiController extends Controller
{

    private $ajax_post = null;

    /**
     * @return bool
     */
    public function beforeExecuteRoute()
    {
        if (!$this->request->isPost()) {
            $this->flash->error(
                "Hi."
            );

            return false;
        }

        $this->ajax_post = $this->request->getJsonRawBody();
    }

    public function deleteAction()
    {
        $id = (int)$this->ajax_post->id;
        /** @var PhoneBook $phone */
        $phone = PhoneBook::findFirst($id);

        if (!$phone) {
            return $this->response->setJsonContent((array) (new ResponseMessage([
                'errors' => true,
                'type' => ResponseMessage::TYPE_DANGER,
                'message' => 'phone item not found',
                ])));
        }

        if (!$phone->setDeleted()->save()) {
            return $this->response->setJsonContent((array) (new ResponseMessage([
                'errors' => true,
                'type' => ResponseMessage::TYPE_DANGER,
                'message' => 'could not delete item'
                ])));
        }

        return $this->response->setJsonContent((array) (new ResponseMessage([
            'type' => ResponseMessage::TYPE_SUCCESS,
            'message' => 'item deleted'
            ])));
    }

    public function saveAction()
    {
        $id = $this->request->getPost('id');

        /** @var PhoneBook $phone_book */
        $phone_book = new PhoneBook();

        if ($id) {
            $phone_book = PhoneBook::find($id);
        }

        if (!$phone_book->load((array)$this->ajax_post->form) || !$phone_book->validation() || !$phone_book->save()) {
            $errors = [];

            foreach ($phone_book->getMessages() as $message) {
                $errors[$message->getField()] = $message->getMessage();
            }

            return $this->response->setJsonContent((array) (new ResponseMessage([
                'type' => ResponseMessage::TYPE_DANGER,
                'message' => 'check form fields',
                'errors' => $errors
            ])));
        }

        return $this->response->setJsonContent((array) (new ResponseMessage([
            'type' => ResponseMessage::TYPE_SUCCESS,
            'message' => 'item saved'
        ])));

    }

    /**
     * Render all list of phone book or filtered by page/search query
     *
     * @return \Phalcon\Http\Response|\Phalcon\Http\ResponseInterface
     */
    public function listAction()
    {

        $query = $this->ajax_post->query ?? '';
        $page =  $this->ajax_post->page ?? 1;
        $limit = $this->ajax_post->limit ?? PhoneBookRepository::PAGE_LIMIT;

        $phone_repository = new PhoneBookRepository(new Paginator(
            [
                'data'  => PhoneBookRepository::findByQuery($query),
                'limit' => $limit,
                'page'  => $page,
            ]
        ));

        $phone_list = $phone_repository->getPaginatedItemsAsArray();

        if (!$phone_list) {
            return $this->response->setJsonContent((array) (new ResponseMessage([
                'type' => ResponseMessage::TYPE_WARNING,
                'message' => "no more records",
            ])));
        }

        return $this->response->setJsonContent((array)(new ResponseMessage([
            'data' => [
                'content' => $phone_list,
                'pages' => $phone_repository->getPages(['first', 'next', 'current', 'before', 'last'])
            ]
        ])));
    }

    /**
     * Shows single record of phone book
     *
     * @return \Phalcon\Http\Response|\Phalcon\Http\ResponseInterface
     */
    public function viewAction()
    {
        $id = (int) $this->request->getPost('id');

        if (!$id) {
            return $this->response->setJsonContent((array)(new ResponseMessage([
                'type' => ResponseMessage::TYPE_DANGER,
                'message' => 'corrupt data',
            ])));
        }

        $phone_item = PhoneBook::find($id)->toArray();

        if (!$phone_item) {
            return $this->response->setJsonContent((array)(new ResponseMessage([
                'type' => ResponseMessage::TYPE_DANGER,
                'message' => 'missed entity',
            ])));
        }

        return $this->response->setJsonContent((array)(new ResponseMessage([
                'data' => ['content' => $phone_item]
            ]
        )));
    }
}