<?php

/**
 * Response helper message class
 *
 * Class ResponseMessage
 */
class ResponseMessage
{
    /**
     * Bootstrap like message type
     * @var string
     */
    public $type;
    /**
     * @var string
     */
    public $message;
    /**
     * Model validation errors
     * @var mixed|[]
     */
    public $errors;
    /**
     * @var mixed|[]
     */
    public $data;

    const TYPE_DANGER = 'danger';
    const TYPE_WARNING = 'warning';
    const TYPE_INFO = 'info';
    const TYPE_SUCCESS = 'success';

    public function __construct(array $params = [])
    {
        foreach ($params as $property => $value) {
            if (property_exists(self::class, $property)) {
                $this->$property = $value;
            }
        }
    }
}