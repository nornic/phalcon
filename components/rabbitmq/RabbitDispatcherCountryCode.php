<?php
namespace app\components\rabbitmq;
use Phalcon\Logger\Adapter\File as FileAdapter;

use app\components\hostaway\CountryCode;

/**
 */
class RabbitDispatcherCountryCode extends AbstractRabbitDispatcherBase {

    const EXCHANGE_TYPE = 'country_code_type';
    const QUEUE_NAME = 'country_code_queue';
    const SEVERITY_TYPE = 'country_code_severity';

    /**
     * @return bool|\Closure
     */
    protected function getCallback()
    {
        return function ($msg) {
            $logger = new FileAdapter('/app/logs/rabbit_country_code.log');
            (new CountryCode())->setLogger($logger)->releaseContent();
        };
    }
}