<?php
namespace app\components\rabbitmq;

use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Connection\AMQPStreamConnection;

/**
 */
abstract class AbstractRabbitDispatcherBase
{
    const EXCHANGE_TYPE = 'default_type';
    const QUEUE_NAME = 'default_queue';
    const SEVERITY_TYPE = 'default_severity';

    public static $instance = null;
    /** @var AMQPStreamConnection $connection */
    protected $connection = null;

    /** @var  AMQPChannel $channel */
    protected $channel = null;

    /**
     * @return mixed
     */
    abstract protected function getCallback();

    /**
     * @return AbstractRabbitDispatcherBase
     */
    public static function i()
    {
        if (is_null(static::$instance)) {
            static::$instance = new static();
        }
        return static::$instance;
    }

    public function setUpServer($params = [])
    {
        $this->connection = new AMQPStreamConnection($params['host'], $params['port'], $params['user'], $params['password']);
        $this->channel = $this->connection->channel();

        $this->channel->exchange_declare(static::EXCHANGE_TYPE, 'direct', false, true, false);
        list($queue_name, ,) = $this->channel->queue_declare(static::QUEUE_NAME, false, true, false, false); // todo-r list($queue_name, ,)?
        $this->channel->queue_bind($queue_name, static::EXCHANGE_TYPE, static::SEVERITY_TYPE);
        echo ' [*] Waiting for logs. To exit press CTRL+C', "\n";


        $this->channel->basic_consume($queue_name, '', false, true, false, false, $this->getCallback());

        while(count($this->channel->callbacks)) {
            $this->channel->wait();
        }

        $this->serverStop();
    }


    public function serverStop()
    {
        if (isset(static::$instance)) {
            return false;
        }
        $this->channel->close();
        $this->connection->close();
    }
}