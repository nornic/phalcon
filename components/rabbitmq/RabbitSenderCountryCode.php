<?php
namespace app\components\rabbitmq;


/**
 */
class RabbitSenderCountryCode extends RabbitSenderBase {
    const EXCHANGE_TYPE = RabbitDispatcherCountryCode::EXCHANGE_TYPE;
    const SEVERITY_TYPE = RabbitDispatcherCountryCode::SEVERITY_TYPE;
}