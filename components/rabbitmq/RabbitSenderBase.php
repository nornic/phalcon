<?php
namespace app\components\rabbitmq;

use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

/**
 *
 * @package app\components\rabbitmq
 */
class RabbitSenderBase {

    const EXCHANGE_TYPE = null;
    const SEVERITY_TYPE = null;

    /** @var AMQPStreamConnection */
    private $connection = null;
    /** @var AMQPChannel */
    private $channel = null;
    private $message = '';

    public $parrams = [];

    public function __construct($params = [])
    {
        $this->createConnection($params);
    }

    private function createConnection($params = []): void
    {
        $this->connection = new AMQPStreamConnection($params['host'], $params['port'], $params['user'], $params['password']);
    }

    public function send()
    {
        if (!$this->connection) {
            $this->createConnection();
        }

        $this->channel = $this->connection->channel();
        $this->channel->exchange_declare(static::EXCHANGE_TYPE, 'direct', false, true, false);
        $msg = new AMQPMessage($this->getMessage(), array('delivery_mode' => 2));
        $this->channel->basic_publish($msg, static::EXCHANGE_TYPE, static::SEVERITY_TYPE);
        $this->channel->close();
    }

    public function setMessage($message)
    {
        $this->message = $message;
        return $this;
    }

    private function getMessage(): string
    {
        return $this->message;
    }
}