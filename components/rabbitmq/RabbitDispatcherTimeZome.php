<?php
namespace app\components\rabbitmq;

use app\components\hostaway\TimeZone;
use Phalcon\Logger\Adapter\File as FileAdapter;


/**
 */
class RabbitDispatcherTimeZome extends AbstractRabbitDispatcherBase {

    const EXCHANGE_TYPE = 'time_zone_type';
    const QUEUE_NAME = 'time_zone_queue';
    const SEVERITY_TYPE = 'time_zone_severity';

    /**
     * @return bool|\Closure
     */
    protected function getCallback()
    {
        return function ($msg) {
            $logger = new FileAdapter('/app/logs/rabbit_time_zone.log');
            (new TimeZone())->setLogger($logger)->releaseContent();
        };
    }
}