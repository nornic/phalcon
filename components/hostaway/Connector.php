<?php
namespace app\components\hostaway;

use Phalcon\Logger\Adapter\File as FileAdapter;


class Connector
{
    const CACHE_DIR = __DIR__ . '/cache/';
    const URL = '';

    private $log_file = null;

    public function setLogger(FileAdapter $file)
    {
        $this->log_file = $file;
        return $this;
    }

    public function logEvent($type = '', $message = '')
    {
        if (!$this->log_file) {
            return $this;
        }

        if (method_exists($this->log_file, $type)) {
            $this->log_file->$type($message);
        }

        return $this;
    }


    public static function getCacheFilename()
    {
        return self::CACHE_DIR.md5(static::class);
    }

    /**
     * Save response from Hostaway API
     *
     * @return string
     */
    public function releaseContent(): string
    {
        $content = '';

        $this->logEvent('log', 'Downloading content');

        try {
            $content = file_get_contents(static::URL);
        } catch (\Exception $exception) {
            $this->logEvent('critical', "error getting file from ".static::URL);
        }
        $is_cache = false;
        if ($content) {
            $this->logEvent('log', 'ok');
            $this->logEvent('log', 'Caching content');
            $is_cache = file_put_contents(static::getCacheFilename(), $content);
        }

        $this->logEvent('log', $is_cache ? 'Content cached' : 'Nothing to cache');

        return $content;
    }

    /**
     * Get cached content or download it by api
     *
     * @return string
     */
    public function getCachedContent(): string
    {
        return file_get_contents(static::getCacheFilename()) ?? $content = $this->releaseContent();
    }

    /**
     * gets hostaway cache|response as array
     *
     * @return array
     */
    public static function getResultKeys(): array
    {
        return array_keys(json_decode((new static())->getCachedContent(), true)['result']) ?? [];
    }


}