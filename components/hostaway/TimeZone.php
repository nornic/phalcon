<?php
namespace app\components\hostaway;

class TimeZone extends Connector
{
    const URL = 'https://api.hostaway.com/timezones';
}