###Total: ~18hrs


Phone book storage CRUD application.

Setup files for local environment
- Vagrantfile `/vm/Vagrantfile`
- docker-compose `/vm/docker-compose` 
- nginx configuration `/vm/vol/nginx.conf`
- public ssh keys (`ssh roo@host.test -p2222`) - `/vm/vol/access_keys`
- cron file `/vm/vol/cron` (schedule actions with remote api). 
- Cron file changes apply by  `docker exec hostaway_sshd sh -c "crontab /etc/cron.d/cron"` in virtual machine

Use `host.test` and ip from `/vm/Vagrantfile` in `/etc/hosts` for app access.
#
Use `vagrant up` from `/vm` folder to start local environment with 
- php-fpm
- nginx
- mysql
- rabbitMQ (scheduler actions with remote api)

#
#####App structure:
- Base point - `/web/index.php`
- Console point - `/cli.php`
- Configuration files - `/config`
- Console commands - `/tasks`
- Components (RabbitMQ && Hostaway connector) - `/components`

 


